#!/usr/bin/env python3

import os
import signal
print("SimpleMon made by Christoph Pomaska is licensed under the GNU General Public License v3.")
print("See the license file for more info.")
print("PID: " + str(os.getpid()) + "\n\n")
print("Starting up...\n")

# Simplemon imports
from sm_console_functions import *
from sm_config import *
from sm_module_functions import *
from sm_scheduling import *
from sm_sqlite import *

def exitall():
    print("Exiting...")
    schedule.schedule_active = False
    exit(0)

def console():
    while (True):
        print('Type "h" or "help" for a list of commands.')
        user_input = input("simplemon console: ")
        if (user_input.lower()[0] == "h"):
            print("Availiable commands:")
            print("(h)elp: Print a list of available commands")
            print("(q)uit: Exit simplemon console")
            print("(c)heck: Run a check manually and print the result")
            print("(s)tatus: Print the status")
            print("(m)monitor: Continually print the status")
            print("(e)mail: Send a test email")
            print("(n)otify: Send a test notification")

        if (user_input.lower()[0] == "q"):
            exitall()
        if (user_input.lower()[0] == "c"):
            print("Not implemented yet")

        if (user_input.lower()[0] == "s"):
            console_status()

        if (user_input.lower()[0] == "m"):
            console_monitor()

def recSIGHUP(signalNumber, frame):
    print("Received SIGHUP")
    print("Reloading config...")

def recSIGQUIT_SIGINT_SIGABRT(signalNumber, frame):
    print("Caught signal " + str(signalNumber))
    exitall()

def recSIGUSR1(signalNumber, frame):
    console_status()

print("Loading config...")
sm_loaded_config = readconfig_main("simplemon.ini")

debug_mode = sm_loaded_config[0]
heartbeat_stdout = sm_loaded_config[3]
heartbeat_log = sm_loaded_config[4]
database_path = sm_loaded_config[5]

if (debug_mode == True):
    print("DEBUG MODE ENABLED")

if __name__ == '__main__':
    signal.signal(signal.SIGHUP, recSIGHUP)
    signal.signal(signal.SIGINT, recSIGQUIT_SIGINT_SIGABRT)
    signal.signal(signal.SIGQUIT, recSIGQUIT_SIGINT_SIGABRT)
    signal.signal(signal.SIGABRT, recSIGQUIT_SIGINT_SIGABRT)
    signal.signal(signal.SIGUSR1, recSIGUSR1)

print("Send SIGUSR1 to simplemon to print status")
print("Send SIGHUP to reload configuration")
print("Send SIGQUIT, SIGABRT or SIGINT (^C or ctrl+C) to stop the simplemon daemon")


if (debug_mode):
   print("Database_path: " + database_path)

sm_sqlite_init(database_path)

schedule = scheduling()
print("Calling heartbeat...")
schedule.start_heartbeat(heartbeat_stdout, heartbeat_log)
print("Calling scheduler...")
schedule.schedule_active = True
schedule.start_scheduling()

enable_console = sm_loaded_config[1]
if (enable_console == False):
    print("Console disabled")
else:
    console()
