#!/usr/bin/env python3
import time
import threading
import schedule
import configparser

class scheduling():
    def __init__(self):
        # This variable is required to add the ability to stop the scheduler again gracefully once it's running.
        schedule_active = True

    def scheduler_loop_thread(self):
        print("Thread is running")
        while(self.schedule_active):
            schedule.run_pending()
            time.sleep(0.5)

    def start_heartbeat(self, heartbeat_stdout, heartbeat_log):
        if (heartbeat_stdout > 0):
            print("Heartbeat to STDOUT active")
            schedule.every(heartbeat_stdout).seconds.do(lambda:print("Heartbeat every " + str(heartbeat_stdout) + " seconds"))
        if (heartbeat_log > 0):
            print("Heartbeat to log active")
            #start_heartbeat(heartbeat_log)

    #def add_schedule(self, check_type, check_interval, check_name):
        #for(len(schedule_array)):
            #schedule.every(check_interval).seconds.do(lambda:print("Check type " + check_type + "named " + check_name + " every "  str(check_interval) + " seconds"))
    
    def start_scheduling(self):
        # Starting heartbeat_thread
        heartbeat_thread = threading.Thread(target=self.scheduler_loop_thread)
        heartbeat_thread.start()

        # Starting regular check_schedule_thread
        check_schedule_thread = threading.Thread(target=self.scheduler_loop_thread)
        check_schedule_thread.start()
