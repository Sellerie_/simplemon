#!/usr/bin/env python3
# This file is part of the simplemon project licensed under GPLv3.

if (__name__ == "__main__"):
    print("This file is not to be run by itself. Please use do_http_check.py instead!")
    exit(1)

import http.client

def v6(host_and_port, v6_has_port):
    if (v6_has_port == True):
        for char_index in range(len(hostandport)):
            if (hostandport[char_index] == "]"):
                host = hostandport[1:char_index]
                port_separator = char_index+2
                port = int(hostandport[port_separator:])
        return(host, port)
    else:
        host = host_and_port
        port = 443
        return(host, port)

def v4_or_hostname(host_and_port):
	v4_with_port = False
	for char_index in range(len(host_and_port)):
		if (host_and_port[char_index] == ":"):
			port_separator = char_index
			v4_with_port =  True
	if (v4_with_port == True):
		host = host_and_port[:port_separator]
		port = int(host_and_port[port_separator+1:])
		return(host, port)
	else:
		host = host_and_port
		port =  443
		return(host, port)

def do_check(host, port, timeout_time, path_behind_hostname, insecure_http):
    if (insecure_http == True):
        http_check = http.client.HTTPConnection(host, port, timeout=timeout_time)
    else:
        http_check = http.client.HTTPSConnection(host, port, timeout=timeout_time)
    
    http_check.request("GET", path_behind_hostname)
    rc = (http_check.getresponse().status)
    return rc

def run_http_check(host_and_port, timeout_time, insecure_http, path_behind_hostname, expected_response_code, v6_has_port):
    if (v6_has_port == True):
        ip_return = v6(host_and_port, v6_has_port)
    else:
        ip_return = v4_or_hostname(host_and_port)

    host = ip_return[0]
    port = ip_return[1]

    actual_response_code = do_check(host, port, timeout_time, path_behind_hostname, insecure_http)

    print(actual_response_code)

    if (host_and_port[0] == "["):
        is_v6_with_port = True
        is_v6_host = True
    else:
        is_v6_with_port = False

    if (actual_response_code == expected_response_code):
        exit(0)
    else:
        exit(1)
