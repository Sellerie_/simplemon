#!/usr/bin/env python3

import argparse
import shutil
import os
import stat

argparser = argparse.ArgumentParser(description='prints total, used and free space of the given mountpoint or mounted drive.')
argparser.add_argument(dest='drive', help="The mountpoint to check") 
argparser.add_argument('-k', '--kibibyte', dest='kibibytes', default=False, action='store_true', help="Print output in kibibytes (KiB)")
argparser.add_argument('-m', '--mebibyte', dest='mebibytes', default=False, action='store_true', help="Print output in mebibytes (MiB)")
argparser.add_argument('-g', '--gibibytes', dest='gibibytes', default=False, action='store_true', help="Print output in gibibytes (GiB)")
argparser.add_argument('-p', '--percent', dest='percent', default=False, action='store_true', help="Additionally print free space in percent")
argparser.add_argument('-P', '--percent-only', dest='percent_only', default=False, action='store_true', help="ONLY print free space in percent")

args = argparser.parse_args()
drive = args.drive
kb = args.kibibytes
mb = args.mebibytes
gb = args.gibibytes
percent = args.percent
percent_only = args.percent_only
total = shutil.disk_usage(drive).total
used = shutil.disk_usage(drive).used
free = shutil.disk_usage(drive).free
unit = "b"

if mb == True and gb == True:
    print("Syntax error: both mebibytes and gibibytes cannot be put out at the same time.")
    exit(1)
if kb == True:
    unit = "k"
if mb == True:
    unit = "m"
if gb == True:
    unit = "g"

# Check if file is block device or mount point
if (os.path.ismount(drive)):
    print("Path is a mount point.")
    path_is_mount_point = True
else:
    print("Path is not a mount point")
    path_is_mount_point = False

if (stat.S_ISBLK(os.stat(drive).st_mode)):
    print("Path is a block device")
    path_is_block_device = True
else:
    print("Path is not a block device")
    path_is_block_device = False

def calculate(func_total, func_used, func_free, func_unit):

    if (unit == "k"):
        func_total_kb = func_total / 1024
        func_used_kb = func_used / 1024
        func_free_kb = func_free / 1024
        return(func_total_kb, func_used_kb, func_free_kb)

    if (unit == "m"):
        func_total_mb = func_total / 1024 / 1024
        func_used_mb = func_used / 1024 / 1024
        func_free_mb = func_free / 1024 / 1024
        return(func_total_mb, func_used_mb, func_free_mb)

    if (unit == "g"):
        func_total_gb = func_total / 1024 / 1024 / 1024
        func_used_gb = func_used / 1024 / 1024 / 1024
        func_free_gb = func_free / 1024 / 1024 / 1024
        return(func_total_gb, func_used_gb, func_free_gb)
    else:
        return(func_total, func_used, func_free)

def putout(space_tuple, print_percent, print_percent_only):
    space_tuple_total = space_tuple[0]
    space_tuple_used = space_tuple[1]
    space_tuple_free = space_tuple[2]

    free_in_percent = space_tuple_free / space_tuple_total * 100

    if (print_percent):
        print(str(space_tuple_total) + " " + str(space_tuple_used) + " " + str(space_tuple_free) + " " + str(int(free_in_percent)) + "%")

    if (print_percent_only):
        print(str(int(free_in_percent)))

    if (print_percent == False) and (print_percent_only == False):
        print(str(space_tuple_total) + " " + str(space_tuple_used) + " " + str(space_tuple_free))

    exit(0)

calculation = calculate(total, used, free, unit)
putout(calculation, percent, percent_only)
