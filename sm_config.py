#!/usr/bin/env python3

#if (__name__ == "__main__"):
#    print("This file is not made to be run by itself!")
#    exit(1)
import configparser
import base64

def get_bool_from_string(tobool_input: str):
    if (tobool_input == "True" or tobool_input == "true"):
        return True
    else:
        return False

def readconfig_main(config_file):
    config = configparser.ConfigParser(interpolation=None)
    config.read(config_file)
    
    # Read config entries
    # debug_mode, enable_console and send_startup_message are boolean
    debug_mode = config['SIMPLEMON']['debug_mode']
    debug_mode = get_bool_from_string(debug_mode)

    enable_console = config['SIMPLEMON']['enable_console']
    enable_console = get_bool_from_string(enable_console)

    send_startup_message = config['SIMPLEMON']['send_startup_message']
    send_startup_message = get_bool_from_string(send_startup_message)

    # heartbeat_stdout_interval and heartbeat_log_interval are integer
    heartbeat_stdout_interval = config['SIMPLEMON']['hb_stdout_interval']
    heartbeat_stdout_interval = int(heartbeat_stdout_interval)

    heartbeat_log_interval = config['SIMPLEMON']['hb_log_interval']
    heartbeat_log_interval = int(heartbeat_log_interval)

    # sqlite database path
    database_path = config['SIMPLEMON']['database_path']

    # Return config entries to config loader
    return(debug_mode, enable_console, send_startup_message, heartbeat_stdout_interval, heartbeat_log_interval, database_path)

def readconfig_smtp(config_file):
    config = configparser.ConfigParser(interpolation=None)
    config.read(config_file)
    smtp_server = config['SMTPCONFIG']['smtp_server']
    print(smtp_server)
    smtp_port = config['SMTPCONFIG']['smtp_port']
    print(smtp_port)

    smtp_auth_user = config['SMTPCONFIG']['smtp_auth_user']
    print(smtp_auth_user)
    smtp_auth_password = config['SMTPCONFIG']['smtp_auth_password']
    print(smtp_auth_password)

    smtp_sender = config['SMTPCONFIG']['smtp_sender']
    print(smtp_sender)
    smtp_recipient = config['SMTPCONFIG']['smtp_recipient']
    print(smtp_recipient)

    smtp_server_mode = "starttls"

    return(smtp_server, smtp_port, smtp_auth_user, smtp_auth_password, smtp_sender, smtp_recipient, smtp_server_mode)
