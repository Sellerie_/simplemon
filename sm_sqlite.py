#!/usr/bin/env python3

import sqlite3
import os
import sys

def connect_database(database_path):
    print(database_path)
    sqlite3_connection = sqlite3.connect(database_path)
    print(sqlite3_connection.cursor())

def populate_new_database(database_path):
    connect_database(database_path)
    print("Preparing new database...")

def check_database_exists(database_path):
    if (os.path.exists(database_path)):
        return True
    else:
        return False

def sm_sqlite_init(database_path):
    if (check_database_exists(database_path)):
        print("Database found: " + database_path)
        connect_database(database_path)
    else:
        user_input = "invalid"
        while (user_input == "invalid"):
            user_input = input("No database found! Create a new one under " + '"' + database_path + '"' + "? (y/N)")
            if (user_input == "Y" or user_input == "y"):
                populate_new_database(database_path)
            else:
                print("No changes have been made.")
                print("Exiting...")
                sys.exit(0)
