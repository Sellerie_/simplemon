#!/usr/bin/env python3

import argparse
import smtplib
import configparser
import base64

argparser = argparse.ArgumentParser(description='Sends an email.')
argparser.add_argument('-c', '--config', dest='config_file', default="simplemon.ini", action='store_true', help="Reads email config from a config file.")

args = argparser.parse_args()
config_file = args.config_file

def readconfig(config_file):
    config = configparser.ConfigParser(interpolation=None)
    config.read(config_file)
    smtp_server = config['SMTP']['smtp_server']
    print(smtp_server)
    smtp_port = config['SMTP']['smtp_port']
    print(smtp_port)

    smtp_auth_user = config['SMTP']['smtp_auth_user']
    print(smtp_auth_user)
    smtp_auth_password = config['SMTP']['smtp_auth_password']
    print(smtp_auth_password)

    smtp_sender = config['SMTP']['smtp_sender']
    print(smtp_sender)
    smtp_recipient = config['SMTP']['smtp_recipient']
    print(smtp_recipient)

    smtp_server_mode = "starttls"

    return(smtp_server, smtp_port, smtp_auth_user, smtp_auth_password, smtp_sender, smtp_recipient, smtp_server_mode)


def send_mail(message_to_send):
    # We have alot to unpack here
    smtp_server = smtp_config[0]
    smtp_port = smtp_config[1]

    smtp_auth_user = smtp_config[2]
    smtp_auth_password = smtp_config[3]
    
    smtp_sender = smtp_config[4]
    smtp_recipient = smtp_config[5]

    smtp_mode = smtp_config[6]


    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()
    server.login(smtp_auth_user, smtp_auth_password)
    server.ehlo()
    server.sendmail(smtp_sender, smtp_recipient, msg)
    server.quit()

try:
    smtp_config
except NameError:
    smtp_config = readconfig(config_file)

msg = "Subject: Test event\n\n Test"
send_mail(msg)

