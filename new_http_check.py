#!/usr/bin/env python3
# This file is part of the simplemon project license under GPLv3

import http.client
import argparse


argparser = argparse.ArgumentParser(description='Checks an HTTP/HTTPS destination for a return code. Example: "https://example.com"')
argparser.add_argument(dest='url', help="The URL to check") 
argparser.add_argument('-t', '--timeout', dest='timeout', type=int, default="10", help="The time in seconds until the check exists with a timeout (defaults to 10)")
argparser.add_argument('-r', '--response', dest='response', type=int, default="200", help="The expected http return code (default is 200)")
argparser.add_argument('-6', dest='v6_host', default=False, action='store_true', help="Assumes the host to check is an ipv6 address. Only required if no port is specified.")
args = argparser.parse_args()

url = args.url
print("URL: " + url)
timeout = args.timeout
expected_response_code = args.response
is_v6_host = args.v6_host

def parse_url(parse_url):
    # Check if protocol is specificed and whether it it http or https
    url_protocol = url.partition("://")[0]
    print("url_protocol: " + url_protocol)

    if (url_protocol != "http") and (url_protocol != "https"):
        print('Error: Invalid or no protocol specified! The protocol prefix has to either start with "https" or "http"!')
        exit(1)

    if (url_protocol == "https"):
        use_https = True
    if (url_protocol ==  "http"):
        use_https = False

    url_no_proto = url.partition("://")[2]
    #print("url_no_proto: " + url_no_proto)

    url_no_path = url_no_proto.partition("/")[0]
    print("url_no_path: " + url_no_path)

    url_path = url_no_proto.partition("/")[2]
    print("url_path: " + url_path)

    url_port = url_no_path.partition(":")[2]
    print("url_port: " + url_port)

    return(use_https, url_no_path, url_port, url_path, timeout)

if (__name__ == "__main__"):
    parsed_url = parse_url(url)
    print(parsed_url)
