#!/usr/bin/env python3

import argparse
from http_check import *

global hostandport
global timeout
global insecure
global path
global response
global is_v6_host
global returncode

argparser = argparse.ArgumentParser(description='Checks an HTTP/HTTPS destination for a return code.')
argparser.add_argument(dest='host', help="The host and port to check (port defaults to 443)") 
argparser.add_argument('-p', '--path', dest='path', default="/", help="The path of the URL to check")
argparser.add_argument('-t', '--timeout', dest='timeout', type=int, default="10", help="The time in seconds until the check exists with a timeout (defaults to 10)")
argparser.add_argument('-i', '--insecure', dest='insecure', default=False, action='store_true', help="Disables https and sends the request via http instead")
argparser.add_argument('-r', '--response', dest='response', type=int, default="200", help="The expected http return code (default is 200)")
argparser.add_argument('-6', dest='v6_host', default=False, action='store_true', help="Assumes the host to check is an ipv6 address. Only required if no port is specified.")
args = argparser.parse_args()

hostandport = args.host
timeout = args.timeout
insecure = args.insecure
path = args.path
expected_response_code = args.response
is_v6_host = args.v6_host

run_http_check(hostandport, timeout, insecure, path, expected_response_code, is_v6_host)
