#!/usr/bin/env python3

import threading
import time
from datetime import datetime

# Get monotonic time to prevent problems with time
def get_monotonic_time():
	return(time.monotonic)

def get_utc_time_for_output():
	now = time.utcnow()
	return("[" + now.strftime("%d") + "/" + now.strftime("%b") + "/" + now.strftime("%Y") + ":" + now.strftime("%H") + ":" + now.strftime("%M") + now.strftime("%S") + "]")
