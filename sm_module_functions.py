#!/usr/bin/env python3

import threading
from http_check import *

# Runs the http check
# host:port, timeout in seconds, insecure(http instead of https), path, expected response code, is ipv6
# example: run_http_check("example.com:443", 10, False, "/", 200, False)
#run_http_check(hostandport, timeout, insecure, path, expected_response_code, is_v6_host)
